package ru.tsc.ichaplygina.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Repository
public class TaskRepository {

    private static final TaskRepository INSTANCE = new TaskRepository();

    public static TaskRepository getInstance() {
        return INSTANCE;
    }

    private final Map<String, Task> tasks = new LinkedHashMap<>();

    {
        write(new Task("TEST1"));
        write(new Task("TEST2"));
        write(new Task("TEST3"));
        write(new Task("TEST4"));
    }

    public void create() {
        write(new Task(("New Task" + System.currentTimeMillis())));
    }

    public Task write(final Task task) {
        tasks.put(task.getId(), task);
        return task;
    }

    public List<Task> write(@NotNull final List<Task> tasks) {
        for (Task task : tasks) this.tasks.put(task.getId(), task);
        return tasks;
    }

    public List<Task> findAll() {
        return new ArrayList<>(tasks.values());
    }

    public Task findById(@NotNull final String id) {
        return tasks.get(id);
    }

    public void removeById(@NotNull final String id) {
        tasks.remove(id);
    }

    public void remove(@NotNull final List<Task> tasks) {
        for (Task task : tasks) this.tasks.remove(task.getId());
    }

    public void removeAll() {
        tasks.clear();
    }

}
