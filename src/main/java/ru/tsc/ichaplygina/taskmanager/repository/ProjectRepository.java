package ru.tsc.ichaplygina.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ProjectRepository {

    private static final ProjectRepository INSTANCE = new ProjectRepository();

    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    private final Map<String, Project> projects = new LinkedHashMap<>();

    {
        write(new Project("TEST1"));
        write(new Project("TEST2"));
        write(new Project("TEST3"));
        write(new Project("TEST4"));
    }

    public void create() {
        write(new Project(("New Project" + System.currentTimeMillis())));
    }

    public Project write(@NotNull final Project project) {
        projects.put(project.getId(), project);
        return project;
    }

    public List<Project> write(@NotNull final List<Project> projects) {
        for (Project project : projects) this.projects.put(project.getId(), project);
        return projects;
    }

    public List<Project> findAll() {
        return new ArrayList<>(projects.values());
    }

    public Project findById(@NotNull final String id) {
        return projects.get(id);
    }

    public void removeById(@NotNull final String id) {
        projects.remove(id);
    }

    public void remove(@NotNull final List<Project> projects) {
        for (Project project : projects) this.projects.remove(project.getId());
    }

    public void removeAll() {
        projects.clear();
    }

}
