package ru.tsc.ichaplygina.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.ichaplygina.taskmanager.api.TaskRestEndpoint;
import ru.tsc.ichaplygina.taskmanager.model.Task;
import ru.tsc.ichaplygina.taskmanager.repository.TaskRepository;

@RestController
@RequestMapping("/api/task")
public class TaskRestEndpointImpl implements TaskRestEndpoint {

    @Autowired
    private TaskRepository taskRepository;

    @Nullable
    @Override
    @PostMapping("/add")
    public Task add(@NotNull @RequestBody final Task task) {
        return taskRepository.write(task);
    }

    @Nullable
    @Override
    @GetMapping("/findById/{id}")
    public Task findById(@NotNull @PathVariable("id") final String id) {
        return taskRepository.findById(id);
    }

    @Override
    @DeleteMapping("/removeById/{id}")
    public void removeById(@NotNull @PathVariable("id") final String id) {
        taskRepository.removeById(id);
    }

    @Override
    @PutMapping("/save")
    public void save(@NotNull @RequestBody final Task task) {
        if (findById(task.getId()) != null) taskRepository.write(task);
    }


}
