package ru.tsc.ichaplygina.taskmanager.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.tsc.ichaplygina.taskmanager")
public class ApplicationConfiguration {

}
