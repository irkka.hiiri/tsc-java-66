package ru.tsc.ichaplygina.taskmanager.api;

import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.tsc.ichaplygina.taskmanager.model.Task;

public interface TaskRestEndpoint {

    @Nullable
    @PostMapping("/add")
    Task add(@RequestBody Task task);

    @Nullable
    @GetMapping("/findById/{id}")
    Task findById(@PathVariable("id") String id);

    @DeleteMapping("/removeById/{id}")
    void removeById(@PathVariable("id") String id);

    @PostMapping("/save")
    void save(@RequestBody Task task);

}
