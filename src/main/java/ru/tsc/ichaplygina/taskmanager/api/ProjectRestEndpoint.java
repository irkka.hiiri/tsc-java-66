package ru.tsc.ichaplygina.taskmanager.api;

import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.tsc.ichaplygina.taskmanager.model.Project;

public interface ProjectRestEndpoint {

    @Nullable
    @PostMapping("/add")
    Project add(@RequestBody Project project);

    @Nullable
    @GetMapping("/findById/{id}")
    Project findById(@PathVariable("id") String id);

    @DeleteMapping("/removeById/{id}")
    void removeById(@PathVariable("id") String id);

    @PostMapping("/save")
    void save(@RequestBody Project project);

}
