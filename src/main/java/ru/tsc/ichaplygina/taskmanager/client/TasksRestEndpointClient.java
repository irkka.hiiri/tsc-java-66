package ru.tsc.ichaplygina.taskmanager.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import ru.tsc.ichaplygina.taskmanager.api.TasksRestEndpoint;
import ru.tsc.ichaplygina.taskmanager.model.Task;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Arrays;
import java.util.List;

public interface TasksRestEndpointClient {

    String BASE_URL = "http://localhost:8080/api/tasks/";

    static TasksRestEndpointClient client() {
        @NotNull final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(TasksRestEndpointClient.class, BASE_URL);
    }

    @PostMapping("add")
    List<Task> add(@RequestBody List<Task> tasks);

    @GetMapping("findAll")
    public List<Task> findAll();

    @PostMapping("remove")
    public void remove(@RequestBody List<Task> tasks);

    @DeleteMapping("removeAll")
    public void removeAll();

    @PutMapping("save")
    public List<Task> save(@RequestBody List<Task> tasks);

}
